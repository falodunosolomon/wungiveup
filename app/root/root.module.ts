import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { SharedModule } from "../shared/shared.module";
import { RootRoutingModule } from "./root-routing.module";
import { RootComponent } from "./root.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        RootRoutingModule,
        SharedModule
    ],
    declarations: [
        RootComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RootModule { }
