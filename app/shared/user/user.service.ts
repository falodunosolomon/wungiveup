import { Injectable } from "@angular/core";
import { User } from "./user.model";

@Injectable()
export class UserService {
    register(user: User) {
        return user;
    }

    login(user: User): boolean {
        return true;
    }

    resetPassword(email) {
        return "";
    }

    handleErrors(error: string) {
        console.error(error);
    }
}
