export interface IUserModel {
    uid?: string;
    name: string;
    email: string;
    emailVerified?: boolean;
    providers?: Array<{ id: string }>;
    anonymous?: boolean;
    phoneNumber: number | string;
    profileImageURL?: null;
    firstName: string;
    dateOfBirth: Date | string;
    id: string;
    avatar: string;
    lastName: string;
    gender: string;
}

export class User implements IUserModel {
    id = "AA00001";
    email = "mariam.yinus@summitech.ng";
    password = "";
    firstName = "Mariam";
    lastName = "Yinus";
    name = "Yinus, Mariam";
    phoneNumber = "+2347032769164";
    type = "";
    dateOfBirth = "30/06/1999";
    avatar = "";
    gender = "Female";
}
