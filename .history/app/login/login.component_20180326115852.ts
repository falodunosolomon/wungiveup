import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { User } from "./../shared/user/user.model";
import { UserService } from "./../shared/user/user.service";

/* ***********************************************************
* Before you can navigate to this page from your app, you need to reference this page's module in the
* global app router module. Add the following object to the global array of routes:
* { path: "login", loadChildren: "./login/login.module#LoginModule" }
* Note that this simply points the path to the page module file. If you move the page, you need to update the route too.
*************************************************************/

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['../app.android.css']
})
export class LoginComponent implements OnInit {

    user: User;
    @ViewChild("email") email: ElementRef;
    @ViewChild("password") password: ElementRef;

    constructor(
        private page: Page,
        private userService: UserService,
        private router: Router,
        private routerExtensions: RouterExtensions) {
        /* ***********************************************************
        * Use the constructor to inject app services that you need in this component.
        *************************************************************/
        this.user = new User();
    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the "ngOnInit" handler to initialize data for this component.
        *************************************************************/
    }

    onLoginWithSocialProviderButtonTap(): void {
        /* ***********************************************************
        * For log in with social provider you can add your custom logic or
        * use NativeScript plugin for log in with Facebook
        * http://market.nativescript.org/plugins/nativescript-facebook
        *************************************************************/
    }

    onSigninButtonTap(): void {
        console.log("this.user  :: " + JSON.stringify(this.user));
        /* ***********************************************************
         * Call your custom sign in logic using the email and password data.
         *************************************************************/
        const email = this.user.email.trim();
        const password = this.user.password.trim();

        if (!email || !password) {
            alert("Please provide both an email address and password.");

            return;
        }
        if (this.userService.login(this.user)) {
            this.routerExtensions.navigate(["/home"], { clearHistory: true });
        } else {
            alert("Wrong username or password, try again.");
        }
    }

    onForgotPasswordTap(): void {
        /* ***********************************************************
        * Call your Forgot Password logic here.
        *************************************************************/

        prompt({
            title: "Forgot Password",
            message: "Enter the email address you used to register for APP NAME to reset your password.",
            defaultText: "",
            okButtonText: "Ok",
            cancelButtonText: "Cancel"
        }).then((data) => {
            if (data.result) {
                // Call the backend to reset the password
                alert({
                    title: "APP NAME",
                    message: "Your password was successfully reset. Please check your email" +
                        "for instructions on choosing a new password.",
                    okButtonText: "Ok"
                });
            }
        });
    }
}
